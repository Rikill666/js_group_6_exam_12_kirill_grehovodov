import React from 'react';
import {useSelector} from "react-redux";
import {Redirect, Route, Switch} from "react-router-dom";
import Posts from "./containers/Cards/Cards";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewCard from "./containers/NewCard/NewCard";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login"/>
);

const Routes = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Switch>
      <Route path="/" exact component={Posts} />
      <Route path="/users" exact component={Posts} />
      <Route path="/users/:id" exact component={Posts} />
      <Route path="/register" exact component={Register} />
      <Route path="/login" exact component={Login} />
      <ProtectedRoute isAllowed={user} path="/cards/new" exact component={NewCard} />
    </Switch>
  );
};

export default Routes;