import {
  CREATE_CARD_ERROR,
  CREATE_CARD_REQUEST,
  CREATE_CARD_SUCCESS,
  FETCH_CARDS_ERROR,
  FETCH_CARDS_REQUEST,
  FETCH_CARDS_SUCCESS
} from "../actions/cardsActions";

const initialState = {
  cards: [],
  error: null,
  createError:null,
  loading: false
};

const cardsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CARDS_REQUEST:
      return {...state, loading: true};
    case FETCH_CARDS_SUCCESS:
      return {...state, cards: action.cards, error:null, loading: false};
    case FETCH_CARDS_ERROR:
      return {...state, error: action.error, loading: false};

    case CREATE_CARD_REQUEST:
      return {...state, loading: true};
    case CREATE_CARD_SUCCESS:
      return {...state, createError:null, loading: false};
    case CREATE_CARD_ERROR:
      return {...state, createError: action.error, loading: false};

    default:
      return state;
  }
};

export default cardsReducer;