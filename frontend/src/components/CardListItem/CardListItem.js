import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import {Card} from "@material-ui/core";
import CardMedia from "@material-ui/core/CardMedia";
import {makeStyles} from "@material-ui/core/styles";
import {apiURL} from "../../constants";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {deleteCard, fetchCards} from "../../store/actions/cardsActions";
import {Link} from "react-router-dom";
import PopUp from "../PopUp/PopUp";

const useStyles = makeStyles({
    card: {
        width: "70%",
        marginLeft: "15%"
    },
    media: {
        paddingTop: '100%', // 16:9
    },
    cardShell: {
        textAlign: 'center',
    },
    link: {
        textDecoration: 'none',
    }
});

const CardListItem = props => {
    const user = useSelector(state => state.users.user);
    const [isOpen, setIsOpen] = useState(false);
    const dispatch = useDispatch();

    const openPopUp =()=>{
        setIsOpen(true);
    };
    const closePopUp =()=>{
        setIsOpen(false);
    };
    const classes = useStyles();
    const image = apiURL + '/' + props.image.replace('\\', '/');
    const cardDelete = (id) => {
        dispatch(deleteCard(id));
        dispatch(fetchCards(props.profileId));
    };
    return (
        <Grid item xs={12} sm={6} md={4}>
            <Card className={classes.card}>
                <CardMedia onClick={openPopUp} image={image} title={props.title} className={classes.media}/>
                <PopUp image={image} isOpen={isOpen} close={closePopUp}/>
                <CardContent>
                    <Typography variant="h4">
                        {props.title}
                    </Typography>

                </CardContent>
                <CardActions>
                    {props.profileId && user && props.profileId === user._id?
                        <Button onClick={() =>cardDelete(props.id)} variant="contained" color="primary">
                            Delete
                        </Button> :!props.profileId?
                        <Typography  component={Link} to={"/users/"+ props.userId} color="primary" variant="h6" className={classes.link}>
                           By: {props.username}
                        </Typography>:null
                    }
                </CardActions>
            </Card>
        </Grid>
    );
};

CardListItem.propTypes = {
    image: PropTypes.string,
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
};

export default CardListItem;