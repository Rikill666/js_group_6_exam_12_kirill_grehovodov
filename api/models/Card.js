const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CardSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
});

const Card = mongoose.model('Card', CardSchema);

module.exports = Card;