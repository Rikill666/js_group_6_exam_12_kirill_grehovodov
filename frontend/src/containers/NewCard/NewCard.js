import React, {Component, Fragment} from 'react';
import CardForm from "../../components/CardForm/CardForm";
import {createCard} from "../../store/actions/cardsActions";
import {connect} from "react-redux";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";

class NewCard extends Component {

    cardCreate = async (cardData) => {
        await this.props.createCard(cardData);
    };

    render() {
        return (
            <Fragment>
                <Toolbar/>
                <Box pb={2} pt={2}>
                    <Typography variant="h4">Add new card</Typography>
                </Box>

                <CardForm
                    error = {this.props.createError}
                    onSubmit={this.cardCreate}
                    tags={this.props.tags}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    createError: state.cards.createError
});

const mapDispatchToProps = dispatch => ({
    createCard: cardData => dispatch(createCard(cardData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewCard);