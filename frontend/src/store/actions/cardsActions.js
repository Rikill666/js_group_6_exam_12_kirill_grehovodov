import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const FETCH_CARDS_REQUEST = 'FETCH_CARDS_REQUEST';
export const FETCH_CARDS_SUCCESS = 'FETCH_CARDS_SUCCESS';
export const FETCH_CARDS_ERROR = 'FETCH_CARDS_ERROR';

export const CREATE_CARD_REQUEST = 'CREATE_CARD_REQUEST';
export const CREATE_CARD_SUCCESS = 'CREATE_CARD_SUCCESS';
export const CREATE_CARD_ERROR = 'CREATE_CARD_ERROR';

export const DELETE_CARD_REQUEST = 'DELETE_CARD_REQUEST';
export const DELETE_CARD_SUCCESS = 'DELETE_CARD_SUCCESS';
export const DELETE_CARD_ERROR = 'DELETE_CARD_ERROR';

export const fetchCardsRequest = () => {return {type: FETCH_CARDS_REQUEST};};
export const fetchCardsSuccess = cards => ({type: FETCH_CARDS_SUCCESS, cards});
export const fetchCardsError = (error) => {return {type: FETCH_CARDS_ERROR, error};};

export const createCardRequest = () => {return {type: CREATE_CARD_REQUEST};};
export const createCardSuccess = () => ({type: CREATE_CARD_SUCCESS});
export const createCardError = (error) => {return {type: CREATE_CARD_ERROR, error};};

export const deleteCardRequest = () => {return {type: DELETE_CARD_REQUEST};};
export const deleteCardSuccess = () => ({type: DELETE_CARD_SUCCESS});
export const deleteCardError = (error) => {return {type: DELETE_CARD_ERROR, error};};

export const deleteCard = (id) => {
  return async dispatch => {
    try {
      dispatch(deleteCardRequest());
      await axiosApi.delete('/cards/' + id);
      dispatch(deleteCardSuccess());
    } catch (e) {
      dispatch(deleteCardError(e));
    }
  };
};

export const fetchCards = (userId) => {
  let url = '/cards';
  if(userId)
  {
    url+=('?user='+ userId);
  }
  return async dispatch => {
    try{
      dispatch(fetchCardsRequest());
      const response = await axiosApi.get(url);
      const cards = response.data;
      dispatch(fetchCardsSuccess(cards));
    }
    catch (e) {
      dispatch(fetchCardsError(e));
    }
  };
};

export const createCard = (cardData) => {
  return async dispatch => {
    try{
      dispatch(createCardRequest());
      await axiosApi.post('/cards', cardData);
      dispatch(createCardSuccess());
      dispatch(push('/'));
    }
    catch (error) {
      if(error.response){
        dispatch(createCardError(error.response.data));
      }
      else{
        dispatch(createCardError({global:"Network error or no internet"}));
      }
    }
  };
};


