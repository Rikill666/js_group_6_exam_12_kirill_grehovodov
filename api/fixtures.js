const mongoose = require('mongoose');
const config = require('./config');
const Cocktail = require('./models/Card');
const User = require('./models/User');
const nanoid = require("nanoid");

const run = async () => {
    await mongoose.connect(config.database, config.databaseOptions);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [user, admin] = await User.create({
        username: 'user',
        password: '123',
        token: nanoid(),
        displayName: "User"
    }, {
        username: 'admin',
        password: '123',
        token: nanoid(),
        displayName: "Admin"
    });


    await Cocktail.create({
            user: user,
            title: "Sunset",
            image: 'uploads/fixtures/sunset.jpg'
        }, {
            user: user,
            title: "Winter",
            image: 'uploads/fixtures/winter.jpg',
        }, {
            user: user,
            title: "Sea",
            image: 'uploads/fixtures/sea.jpg'
        },
        {
            user: admin,
            title: "Spring",
            image: 'uploads/fixtures/spring.jpg'
        },
        {
            user: admin,
            title: "Sahara",
            image: 'uploads/fixtures/sahara.jpg'
        }
    );

    mongoose.connection.close();
};

run().catch(e => {
    mongoose.connection.close();
    throw e;
});