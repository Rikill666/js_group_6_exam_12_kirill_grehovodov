import React from 'react';
import PropTypes from 'prop-types';
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FileInput from "./FileInput";


const FormElement = props => {


    let inputChildren = undefined;

    if (props.type === 'select') {
        inputChildren = props.options.map(o => (
            <MenuItem key={o.id} value={o.id}>
                {o.title}
            </MenuItem>
        ));
    }

    let inputComponent = (
        <TextField
            fullWidth
            variant="outlined"
            label={props.title}
            error={!!props.error}
            type={props.type}
            select={props.type === 'select'}
            name={props.propertyName} id={props.propertyName}
            value={props.value}
            onChange={props.onChange}
            required={props.required}
            autoComplete={props.autoComplete}
            placeholder={props.placeholder}
            children={inputChildren}
            helperText={props.error}
        >
            {inputChildren}
        </TextField>
    );

    if (props.type === 'file') {
        inputComponent = (
            <FileInput
                error={!!props.error}
                helperText={props.error}
                label={props.title}
                name={props.propertyName}
                onChange={props.onChange}
            />
        )
    }

    return inputComponent;
};

FormElement.propTypes = {
    propertyName: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    required: PropTypes.bool,
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array]),
    options: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
};

export default FormElement;