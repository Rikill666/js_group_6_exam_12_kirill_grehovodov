import React, {Component} from 'react';
import {fetchCards} from "../../store/actions/cardsActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import CardListItem from "../../components/CardListItem/CardListItem";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import 'react-image-lightbox/style.css';


class Cards extends Component {

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.fetchCards(this.props.match.params.id);
        } else {
            this.props.fetchCards();
        }
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.match.params.id){
            this.props.fetchCards(this.props.match.params.id);
        }
        else{
            this.props.fetchCards();
        }
    }

    render() {
        return (
            <Grid container direction="column" spacing={2}>
                <Grid item container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                        {this.props.match.params.id && this.props.cards.length>0?
                            <Typography variant="h4">
                                {this.props.cards[0].user.displayName || this.props.cards[0].user.username} gallery
                            </Typography> : null
                        }
                    </Grid>
                    <Grid item>
                        {
                            this.props.user && this.props.match.params.id && this.props.user._id === this.props.match.params.id ?
                                <Button
                                    color="primary"
                                    component={Link}
                                    to={"/cards/new"}
                                >
                                    <Typography variant="h6">
                                        Add new photo
                                    </Typography>

                                </Button> : null
                        }
                    </Grid>
                </Grid>
                <Grid item container direction="row" spacing={2} justify="center">
                    {this.props.cards.map(card => {
                        return <CardListItem
                            key={card._id}
                            title={card.title}
                            id={card._id}
                            image={card.image}
                            userId = {card.user._id}
                            username={card.user.displayName || card.user.username}
                            profileId={this.props.match.params.id || null}
                        />
                    })}
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => ({
    cards: state.cards.cards,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchCards: (userId) => dispatch(fetchCards(userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
