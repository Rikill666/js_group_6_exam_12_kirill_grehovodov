const express = require('express');
const ValidationError = require('mongoose').Error.ValidationError;
const auth = require('../middleware/auth');
const upload = require('../multer').uploads;
const User = require('../models/User');
const Card = require('../models/Card');

const router = express.Router();

router.get('/', async (req, res) => {
    let cards;
    if (req.query.user) {
        cards = await Card.find({user: req.query.user}).populate("user","username displayName _id");
    }
    else{
        cards = await Card.find().populate("user","username displayName");
    }
    res.send(cards);
});


router.post('/', [auth, upload.single('image')], async (req, res) => {
    try {
        const user = req.user;
        const cardData = {
            user: user,
            title:req.body.title,
        };
        if (req.file) {
            cardData.image = req.file.filename;
        }
        const card = new Card(cardData);
        await card.save();
        return res.send({id: card._id});
    } catch (e) {
        if (e instanceof ValidationError) {
            return res.status(400).send(e);
        } else {
            console.log(e);
            return res.sendStatus(500);
        }
    }
});

router.get("/:id", async (req, res) => {
    const id = req.params.id;
    if(id){
        try {
            const card = await Card.findOne({_id: id});
            if (!card) {
                return res.status(404).send({message: "Card not found"});
            }
            return res.send(card);
        }
        catch(error){
            return res.status(400).send(error);
        }
    }
});

router.delete('/:id', auth, async (req, res) => {
    const id = req.params.id;
    try {
        const card = await Card.findOne({_id: id});
        if(!card){
            return res.status(404).send("No this cocktail");
        }
        await Card.deleteOne({_id: id});
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;